#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <exception>
#include <iostream>

class Random
{
public:
    static float getFloat(const float min = 0.0f, const float max = 1.0f)
    { return m_distribution(m_gen) * (max - min) + min; }

private:
    static std::mt19937 m_gen;
    static std::uniform_real_distribution<float> m_distribution;
};
std::mt19937 Random::m_gen = std::mt19937(std::random_device()());
std::uniform_real_distribution<float> Random::m_distribution = std::uniform_real_distribution<float>(0.0f, 1.0f);

class Timer
{
public:
    static void start()
    { m_start = std::chrono::system_clock::now(); }

    static auto stop()
    {
        m_stop = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(m_stop - m_start);
        return duration.count();
    }
private:
    static std::chrono::time_point<std::chrono::system_clock> m_start;
    static std::chrono::time_point<std::chrono::system_clock> m_stop;
};
std::chrono::time_point<std::chrono::system_clock> Timer::m_start = std::chrono::system_clock::now();
std::chrono::time_point<std::chrono::system_clock> Timer::m_stop = std::chrono::system_clock::now();

class Matrix
{
public:
    static Matrix create(const size_t nRows, const size_t nColumns, bool init = true)
    { 
        auto result = Matrix(nRows, nColumns);
        if(!init) {
            return result;
        }

        auto size = result.getSize();
        for(size_t i = 0; i < size; ++i) {
            result[i] = Random::getFloat();
        }
        return result;
    }

    static Matrix copy(const Matrix& orig)
    {
        Matrix result(orig.getNRows(), orig.getNColumns());
        auto size = result.getSize();
        for(size_t i = 0; i < size; ++i) {
            result[i] = orig[i];
        }
        return result;
    }

    size_t getNRows() const
    { return m_nRows; }
    
    size_t getNColumns() const
    { return m_nColumns; }

    size_t getSize() const
    { return m_nColumns * m_nRows; }

    float& operator[](const size_t idx)
    { return m_data[idx]; }
    
    const float& operator[](const size_t idx) const
    { return m_data[idx]; }

private:
    Matrix(const size_t nRows, const size_t nColumns):
        m_nRows{ nRows },
        m_nColumns{ nColumns },
        m_data{ std::make_unique<float[]>(nRows * nColumns) }
    {}

private:
    std::unique_ptr<float[]> m_data;
    size_t m_nRows;
    size_t m_nColumns;

public:
    friend Matrix matmulBlocks(Matrix& a, Matrix& b);
};

float matDiff(const Matrix& a, const Matrix& b)
{
    if(a.getNRows() != b.getNRows() || a.getNColumns() != b.getNColumns()) {
        throw std::invalid_argument("dimensions not compatible");
    }
    
    const auto size = a.getSize();

    float sum_delta = 0, sum = 0;
    for (auto i = 0u; i < size; ++i) {
        sum_delta += std::abs(a[i] - b[i]);
        sum += std::abs(a[i]) + std::abs(b[i]);
    }
    return sum_delta / sum;
} 

Matrix matmulSimple(const Matrix& a, const Matrix& b)
{
    if(a.getNColumns() != b.getNRows()) {
        throw std::invalid_argument("dimensions not compatible");
    }

    const auto resNRows = a.getNRows();
    const auto resNColumns = b.getNColumns();

    auto res = Matrix::create(resNRows, resNColumns, false);

    const auto multLen = a.getNColumns();
    for(auto r = 0u; r < resNRows; ++r) {
        for(auto c = 0u; c < resNColumns; ++c) {
            res[r * resNColumns + c] = 0;
            for(auto i = 0u; i < multLen; ++i) {
                res[r * resNColumns + c] += a[r * multLen + i] * b[i * multLen + c];
            }
        }
    }
    return res;
}

Matrix matmulByRows(const Matrix& a, const Matrix& b)
{
    if(a.getNColumns() != b.getNRows()) {
        throw std::invalid_argument("dimensions not compatible");
    }

    const auto resNRows = a.getNRows();
    const auto resNColumns = b.getNColumns();

    auto res = Matrix::create(resNRows, resNColumns, false);

    const auto multLen = a.getNColumns();
    for(auto r = 0u; r < resNRows; ++r) {
        for(auto c = 0u; c < resNColumns; ++c) {
            res[r * resNColumns + c] = 0;
        }
        for(auto i = 0u; i < multLen; ++i) {
            for(auto c = 0u; c < resNColumns; ++c) {
                res[r * resNColumns + c] += a[r * multLen + i] * b[i * resNColumns + c];
            }
        }
    }

    return res;
}

Matrix matmulBlocks(Matrix& a, Matrix& b)
{
    if(a.getNColumns() != b.getNRows()) {
        throw std::invalid_argument("dimensions not compatible");
    }

    const auto resNRows = a.getNRows();
    const auto resNColumns = b.getNColumns();
    const auto multLen = a.getNColumns();

    if(!((resNRows % 4 == 0) && (resNColumns % 4 == 0) && (multLen % 4 == 0))) {
        throw std::invalid_argument("can't divide matricies into blocks");
    }

    const auto nBlocksRow = resNRows / 4;
    const auto nBlocksColumn = resNColumns / 4;
    const auto blocksMultLen = multLen / 4;

    struct float4_t {
        float data[4];
        float& operator[](size_t idx) { return data[idx]; }
        const float& operator[](size_t idx) const { return data[idx]; }
        float4_t operator*(float v) const 
        {
            float4_t res;
            for(auto i = 0u; i < 4; ++i) {
                res[i] = v * data[i];
            }
            return res;
        }
        float4_t operator+(const float4_t& rhs) const
        {
            float4_t res;
            for(auto i = 0u; i < 4; ++i) {
                res[i] = rhs[i] + data[i];
            }
            return res;
        }
    };  

    const auto blockReorderLhs = [&nBlocksRow, &nBlocksColumn, &blocksMultLen](float4_t *in, float4_t *out) {
        for(auto br = 0u; br < nBlocksRow; ++br) {
            for(auto bc = 0u; bc < blocksMultLen; ++bc) {
                float4_t *offs = in + bc + 4 * blocksMultLen * br;
                for (auto i = 0u; i < 4; ++i)
                    *(out++) = offs[i * blocksMultLen];
            }
        }
    };

    const auto blockReorderRhs = [&nBlocksRow, &nBlocksColumn, &blocksMultLen](float4_t *in, float4_t *out) {
        for(auto bc = 0u; bc < nBlocksColumn; ++bc) {
            for(auto br = 0u; br < blocksMultLen; ++br) {
                float4_t *offs = in + bc + 4 * nBlocksColumn * br;
                for (auto i = 0u; i < 4; ++i)
                    *(out++) = offs[i * nBlocksColumn];
            }
        }
    };
    
    const auto blockReorderRes = [&nBlocksRow, &nBlocksColumn, &blocksMultLen](float4_t *in, float4_t *out) {
        for(auto bc = 0u; bc < nBlocksColumn; ++bc) {
            for(auto br = 0u; br < nBlocksRow; ++br) {
                float4_t *offs = out + bc + 4 * nBlocksColumn * br;
                for (auto i = 0u; i < 4; ++i)
                    offs[i * nBlocksColumn] = *(in++);
            }
        }
    };

    auto tmpLhs = Matrix::create(a.getNColumns(), a.getNRows(), false);
    blockReorderLhs(reinterpret_cast<float4_t*>(a.m_data.get()), reinterpret_cast<float4_t*>(tmpLhs.m_data.get()));

    auto tmpRhs = Matrix::create(b.getNColumns(), b.getNRows(), false);
    blockReorderRhs(reinterpret_cast<float4_t*>(b.m_data.get()), reinterpret_cast<float4_t*>(tmpRhs.m_data.get()));

    auto tmpRes = Matrix::create(resNRows, resNColumns, false);
    for(auto br = 0u; br < nBlocksRow; ++br) {
        for(auto bc = 0u; bc < nBlocksColumn; ++bc) {
            auto r_ptr = reinterpret_cast<float4_t*>(tmpLhs.m_data.get() + 16 * blocksMultLen * br);
            auto c_ptr = reinterpret_cast<float4_t*>(tmpRhs.m_data.get() + 16 * blocksMultLen * bc);
            auto res_ptr = reinterpret_cast<float4_t*>(tmpRes.m_data.get() + 16 * (br + bc * nBlocksRow));

            float4_t tmp[4], tmp2[4];
            for(auto i = 0u; i < 4; ++i) {
                tmp[i] = {0, 0, 0, 0};
            }

            for(auto i = 0u; i < 4 * blocksMultLen; i += 4) {
                for(auto j = 0u; j < 4; ++j) {
                    tmp2[j] = c_ptr[i + j];
                }
                for(auto j = 0u; j < 4; ++j) {
                    for(auto k = 0u; k < 4; ++k) {
                        tmp[j] = tmp[j] + tmp2[k] * r_ptr[i + j][k];
                    }
                }
            }

            for(auto i = 0u; i < 4; ++i) {
                res_ptr[i] = tmp[i];
            }
        }
    }

    auto res = Matrix::create(resNRows, resNColumns, false);
    blockReorderRes(reinterpret_cast<float4_t*>(tmpRes.m_data.get()), reinterpret_cast<float4_t*>(res.m_data.get()));
    return res;
}

int main(int argc, char *argv[])
{
    const auto matDimSize = 1024;

    auto a = Matrix::create(matDimSize, matDimSize);
    auto b = Matrix::create(matDimSize, matDimSize);

    auto timeSimple = 0;
    Timer::start();
    auto resSimple = matmulSimple(a, b);
    timeSimple = Timer::stop();

    auto timeByRows = 0;
    Timer::start();
    auto resByRows = matmulByRows(a, b);
    timeByRows = Timer::stop();

    auto timeBlocks = 0;
    Timer::start();
    auto resBlocks = matmulBlocks(a, b);
    timeBlocks = Timer::stop();

    std::cout << "Simple " << timeSimple << "ms.\n";
    std::cout << "ByRows " << timeByRows << "ms. diff with simple " << matDiff(resSimple, resByRows) << "\n";
    std::cout << "ByBlocks " << timeBlocks << "ms. diff with simple " << matDiff(resSimple, resBlocks) << "\n";
    return 0;
}
